package jpplugin.jpsnippets;

import com.tom.jpedit.gui.JPEditWindow;
import com.tom.jpedit.plugins.*;
import com.tom.jpedit.plugins.components.PluginKeyboardShortcut;
import com.tom.jpedit.plugins.components.PluginMenuItem;
import com.tom.jpedit.plugins.components.PluginToolbarButton;
import javafx.event.EventHandler;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;

public class JPSnippets implements JPEditPlugin {
  HashMap<String, String> substitutions;

  @Override
  public void onPluginLoad(@NotNull List<JPEditWindow> list) throws Exception {
    try (BufferedReader reader = new BufferedReader(new FileReader(getSnippetsFile()))) {
      substitutions = new HashMap<>();
      reader.lines().map(s -> s.split("=")).forEach(p -> substitutions.put(p[0], p[1]));
    }
    for (JPEditWindow window : list) {
      window.getTextArea().addEventHandler(KeyEvent.KEY_PRESSED, getKeyEventEventHandler());
    }
  }

  protected File getSnippetsFile() throws IOException {
    return JPPluginAPI.getFileForPlugin(JPSnippets.class, Paths.get("snippets.ini"));
  }

  @Contract(pure = true)
  private @NotNull EventHandler<KeyEvent> getKeyEventEventHandler() {
    return event -> {
      if (event.getCode() == KeyCode.TAB) {
        TextArea area = (TextArea) event.getTarget();
        boolean didReplace = false;
        for (var pair : substitutions.entrySet()) {
          var pattern = pair.getKey();
          var replacement = pair.getValue();
          var patternLength = pattern.length();
          int caretPosition = area.getCaretPosition();
          try {
            String substring = area.getText(caretPosition - patternLength, caretPosition);
            if (substring.equals(pattern)) {
              area.replaceText(caretPosition - patternLength, caretPosition, replacement);
              didReplace = true;
              break;
            }
          } catch (IndexOutOfBoundsException oob) {
            // ignore this because text might not be long enough yet
          }
        }
        if (didReplace) {
          event.consume();
        }
      }
    };
  }

  @Override
  public void onNewWindow(@NotNull List<JPEditWindow> list, JPEditWindow jpEditWindow) {
    jpEditWindow.getTextArea().addEventHandler(KeyEvent.KEY_PRESSED, getKeyEventEventHandler());
  }

  @Override
  public PluginProperties pluginProperties() {
    PluginMenuItem snippets = new PluginMenuItem("Snippets");
    snippets.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
      SnippetsWindow stage = new SnippetsWindow(null, this);
      stage.show();
    });

    PluginKeyboardShortcut shortcut = new PluginKeyboardShortcut(true, true, false, false, false, KeyCode.S) {
      @Override
      public void handle(KeyEvent keyEvent) {
        SnippetsWindow stage = new SnippetsWindow(null, JPSnippets.this);
        stage.show();
      }
    };

    PluginToolbarButton button = new PluginToolbarButton("Snippets");
    button.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> {
      SnippetsWindow stage = new SnippetsWindow(null, this);
      stage.show();
    });

    return new PluginProperties(snippets, shortcut, button);
  }

  @Override
  public void onWindowClose(@NotNull List<JPEditWindow> windows, @NotNull JPEditWindow closingWindow) {
    JPEditPlugin.super.onWindowClose(windows, closingWindow);
  }

  @Override
  public void onRemoved(List<JPEditWindow> windows) {
    onExit();
  }

  @Override
  public void onExit() {
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(getSnippetsFile()))) {
      substitutions.forEach((k, replacement) -> {
        try {
          writer.write(k + "=" + replacement + System.lineSeparator());
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      });
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
