package jpplugin.jpsnippets;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Substitution {
  public String pattern;
  public String replacement;

  public Substitution(String key, String value) {
    this.pattern = key;
    this.replacement = value;
  }

  public static List<Substitution> fromMap(Map<String, String> map) {
    List<Substitution> subs = new ArrayList<>();
    for (Map.Entry<String, String> entry : map.entrySet()) {
      subs.add(new Substitution(entry.getKey(), entry.getValue()));
    }
    return subs;
  }

  @Override
  public String toString() {
    return "Substitution{" +
        "pattern='" + pattern + '\'' +
        ", replacement='" + replacement + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Substitution that = (Substitution) o;
    return Objects.equals(pattern, that.pattern) && Objects.equals(replacement, that.replacement);
  }

  @Override
  public int hashCode() {
    return Objects.hash(pattern, replacement);
  }

  public String getPattern() {
    return pattern;
  }

  public void setPattern(String pattern) {
    this.pattern = pattern;
  }

  public String getReplacement() {
    return replacement;
  }

  public void setReplacement(String replacement) {
    this.replacement = replacement;
  }
}
