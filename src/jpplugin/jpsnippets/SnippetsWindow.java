package jpplugin.jpsnippets;

import com.tom.jpedit.gui.DependableStage;
import com.tom.jpedit.gui.DependantStage;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.Map;

public class SnippetsWindow extends DependantStage {
  TableView<Substitution> snippetsTable;
  Button addButton;
  Button deleteButton;
  Button closeButton;

  ObservableList<Substitution> substitutions;

  public SnippetsWindow(
      DependableStage owner,
      JPSnippets context
  ) {
    super(owner);
    substitutions = FXCollections.observableList(Substitution.fromMap(context.substitutions));
    this.snippetsTable = new TableView<>(substitutions);
    this.addButton = new Button("Add");
    this.deleteButton = new Button("Delete");
    this.closeButton = new Button("Close");

    snippetsTable.addEventHandler(MouseEvent.ANY, event -> {
      deleteButton.setDisable(snippetsTable.getSelectionModel().getSelectedItem() == null);
    });

    TableColumn<Substitution, Object> pattern = new TableColumn<>("Pattern");
    pattern.setCellValueFactory(new PropertyValueFactory<>("pattern"));
    snippetsTable.getColumns().add(pattern);
    TableColumn<Substitution, Object> replacement = new TableColumn<>("Replacement");
    replacement.setCellValueFactory(new PropertyValueFactory<>("replacement"));
    snippetsTable.getColumns().add(replacement);

    VBox root = new VBox();
    HBox buttons = new HBox();

    root.setSpacing(5.0);
    buttons.setSpacing(5.0);
    root.setPadding(new Insets(5));
    buttons.setPadding(new Insets(5));

    deleteButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
      var item = snippetsTable.getSelectionModel().getSelectedItem();
      if (item == null) {
        return;
      }
      snippetsTable.getItems().remove(item);
      snippetsTable.getSelectionModel().clearSelection();
    });

    addButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
      var status = new Object() {
        boolean accepted = false;
      };
      Stage stage = new Stage();
      Label patLabel = new Label("Pattern");
      Label replLabel = new Label("Replacement");
      TextField patField = new TextField();
      TextField replField = new TextField();
      Button goButton = new Button("Accept");
      goButton.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
        status.accepted = true;
        stage.close();
      });
      GridPane grid = new GridPane();
      grid.setPadding(new Insets(5));
      grid.setHgap(5.0);
      grid.setVgap(5.0);
      grid.add(patLabel, 0, 0);
      grid.add(replLabel, 0, 1);
      grid.add(patField, 1, 0);
      grid.add(replField, 1, 1);
      grid.add(goButton, 1, 2);
      Scene scene = new Scene(grid);
      stage.setScene(scene);
      stage.showAndWait();

      if (status.accepted) {
        Substitution sub = new Substitution(patField.getText(), replField.getText());
        substitutions.add(sub);
        context.substitutions.put(patField.getText(), replField.getText());
      }
    });

    closeButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> SnippetsWindow.this.close());

    root.getChildren().addAll(snippetsTable, buttons, closeButton);
    buttons.getChildren().addAll(deleteButton, addButton);
    Scene scene = new Scene(root);
    setScene(scene);
  }
}
