module JPSnippets {
  requires jpedit;
  requires javafx.base;
  requires javafx.controls;
  requires java.logging;
  exports jpplugin.jpsnippets;
  opens jpplugin.jpsnippets to javafx.base, jpedit;
}